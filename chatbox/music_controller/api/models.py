from pyexpat import model
from tokenize import group
from turtle import title
from django.db import models
import string
import random
from django.contrib.auth.models import User
from django.utils import timezone


# def generate_unique_code():
#     while True:
#         code = ''.join(random.choices(string.ascii_uppercase, k = 6))
#         if Room.objects.filter(code=code).count() == 0:
#             break
#     return code

# sender = models.ForeignKey(User, on_delete=models.CASCADE, related_name='sender')


# Create your models here.
class UserInfo(models.Model):
    GENDER_CHOICES = (
        ("M", "Male"),
        ("F", "Female"),
        ("O", "Other"),
    )
    STATUS_CHOICES = (
        ("1", "active"),
        ("0", "inactive"),
    )
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user')
    contact_no = models.IntegerField()
    gender = models.CharField(max_length=8,default=False,choices=GENDER_CHOICES)
    status = models.CharField(max_length=8,default=False,choices=STATUS_CHOICES)
    created_at = models.DateTimeField(auto_now_add=True)

    def __int__(self):
        return self.id

class GroupData(models.Model):
    STATUS_CHOICES = (
        ("1", "active"),
        ("0", "inactive"),
    )
    title = models.CharField(max_length=40,default=False,null=None)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='admin')
    status = models.CharField(max_length=8,default=False,choices=STATUS_CHOICES)
    created_at = models.DateTimeField(auto_now_add=True)

    def __int__(self):
        return self.id
    
class GroupMember(models.Model):
    STATUS_CHOICES = (
        ("1", "active"),
        ("0", "inactive"),
    )
    group = models.ForeignKey(GroupData, on_delete=models.CASCADE, related_name='group')
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='group_member')
    status = models.CharField(max_length=8,default=False,choices=STATUS_CHOICES)
    created_at = models.DateTimeField(auto_now_add=True)
    def __int__(self):
        return self.id
    

class ChatBox(models.Model):
    STATUS_CHOICES = (
        ("1", "active"),
        ("0", "inactive"),
    )
    sender = models.ForeignKey(User, on_delete=models.CASCADE, related_name='sender')
    reciever = models.IntegerField(default=0)
    message = models.TextField(blank = True)
    group = models.IntegerField(default=0)
    status = models.CharField(max_length=8,default=False,choices=STATUS_CHOICES)
    created_at = models.DateTimeField(default=timezone.now)
    def __int__(self):
        return self.id

    class Meta:
        verbose_name_plural = 'chatBox'
    
    @staticmethod
    def get_chat_data():
        return ChatBox.objects.all()
    
    

