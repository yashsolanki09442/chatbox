from django.contrib import admin
from .models import *

# Register your models here.
class UserInfoAdmin(admin.ModelAdmin):
    pass
admin.site.register(UserInfo, UserInfoAdmin)

class GroupDataAdmin(admin.ModelAdmin):
    pass
admin.site.register(GroupData, GroupDataAdmin)

class GroupMemberAdmin(admin.ModelAdmin):
    pass
admin.site.register(GroupMember, GroupMemberAdmin)

class ChatBoxAdmin(admin.ModelAdmin):
    pass
admin.site.register(ChatBox, ChatBoxAdmin)



