from unicodedata import name
from django.urls import path,include
from . import views

urlpatterns = [
    path('', views.UserData.index, name=''),
    path('registeredData', views.UserData.registerUser, name='registeredData'),
    path('login', views.UserData.Login, name='login'),
    path('home', views.UserData.home, name='home'),
    path('getChat', views.UserData.getChat, name='getChat'),
    path('pushChat', views.UserData.pushChat, name='pushChat'),
    path('addGroup', views.UserData.addGroup, name='addGroup'),
    path('showGroup', views.UserData.showGroup, name='showGroup'),
]