import json
import os
from traceback import print_tb
from django.db.models import Q
from django.dispatch import receiver
from django.shortcuts import redirect, render
from .serializers import *
from .models import *
from .forms import *
from rest_framework import generics
from rest_framework.views import APIView
from django.http import JsonResponse,HttpResponse
from django.views import View
from pathlib import Path
from django.contrib import messages
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.models import User
from django.views.decorators.csrf import csrf_exempt
from django.db.models import Count

# Create your views here. 
class UserData(View):
    def index(request):
        data={}
        return render(request, "register.html", data)
    
    def registerUser(request):
        if request.method == 'POST':
                user = User.objects.create_user(username=request.POST['username'],
                                 email=request.POST['username'],
                                 password=request.POST['password'])
                user.first_name = request.POST['first_name']
                user.last_name = request.POST['last_name']
                user.save()
                messages.success(request, f'Your account has been created ! You are now able to log in')
                return redirect('login')
        else:
            form = UserRegisterForm()
        return render(request, 'register.html', {'form': form, 'title':'register here'})
    
    def Login(request):
        if request.method == 'POST':
            username = request.POST['username']
            password = request.POST['password']
            user = authenticate(request, username = username, password = password)
            if user is not None:
                form = login(request, user)    
                request.session["user_id"] = user.id
                request.session["username"] = username
                messages.success(request, f' welcome {username} !!')
                return redirect('home')
            else:
                messages.info(request, f'account done not exit plz sign in')
        form = AuthenticationForm()
        return render(request, 'login.html', {'form':form, 'title':'log in'})
    
    def home(request):
        groupList = GroupData.objects.all()
        userList = User.objects.all().exclude(id__in=[request.session['user_id']]).values('id','username','first_name','last_name')
        data = {
            'username': request.session["username"],
            'groupList': groupList,
            'userList': userList
        }
        return render(request, 'app-chat.html', data)


    @csrf_exempt
    def getChat(request):
        if 'user_id' in  request.session:
            if request.POST.get:
                if request.POST.get('groupId') == '0':
                    if ChatBox.objects.filter(sender=request.session['user_id'],reciever=request.POST.get('id'),group=request.POST.get('groupId')).count() > 0 or ChatBox.objects.filter(reciever=request.session['user_id'],sender=request.POST.get('id'),group=request.POST.get('groupId')).count() > 0:
                        chat = ChatBox.objects.filter(
                                Q(sender=request.session['user_id'],reciever=request.POST.get('id'),group=request.POST.get('groupId')) |
                                Q(reciever=request.session['user_id'],sender=request.POST.get('id'),group=request.POST.get('groupId'))
                            ).order_by('created_at')
                        return JsonResponse({"data":list(chat.values())}, safe=False)
                    else:
                        return JsonResponse({"data":[]}, safe=False)
                else:

                    chat = (ChatBox.objects
                        .filter(group=request.POST.get('groupId'))
                        .order_by('created_at'))
                    return JsonResponse({"data":list(chat.values())}, safe=False)    
        return redirect('login')
    
    @csrf_exempt   
    def pushChat(request):
        if request.POST.get:
            sender = User.objects.get(id=request.POST.get('sender'))
            if request.POST.get('group') == '0':
                chat = ChatBox.objects.create(sender=sender,reciever=request.POST.get('reciever'),message=request.POST.get('message'),group=request.POST.get('group'))
            else:
                chat = ChatBox.objects.create(sender=sender,message=request.POST.get('message'),group=request.POST.get('group'))
            return JsonResponse({'data':'Success'},safe=False)
    
    @csrf_exempt   
    def addGroup(request):
        if request.POST.get:
            user = User.objects.get(id=request.POST.get('user'))
            group = GroupData.objects.create(title=request.POST.get('title'),user = user)
            group_member = GroupMember.objects.create(group=group,user=user)
            for i in request.POST.getlist('member[]'):
                user = User.objects.get(id=i)
                group_member = GroupMember.objects.create(group=group,user=user)
            return JsonResponse({'data':'Success'},safe=False)
    
    @csrf_exempt   
    def showGroup(request):
        if request.POST.get:
            userData=[]
            group = GroupData.objects.get(id=request.POST.get('group'))
            group_member = GroupMember.objects.filter(group=group)
            for i in group_member:
                user = User.objects.get(username=i.user)
                userData.append(user.first_name+" "+user.last_name)
            return JsonResponse({"data":userData}, safe=False)
        return JsonResponse({'data':'Success'},safe=False)